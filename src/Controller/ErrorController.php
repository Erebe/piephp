<?php

namespace Controller;

class ErrorController extends \Core\Controller {
  public function fileNotFoundAction() {
    $this->render('404');
  }
}

?>
