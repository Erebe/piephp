<?php

namespace Core;

class Request {
  public static function get_request() {
    return array_map('htmlspecialchars', array_map('stripslashes',
      array_map('trim', $_REQUEST)));
  }
}

?>
