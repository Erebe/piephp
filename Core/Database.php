<?php

namespace Core;

class Database {
  private static $database;

  public function __construct() {
  }

  public static function get() {
    if (isset(self::$database))
      return self::$database;
    try {
      self::$database =
        new \PDO('mysql:host=localhost;dbname=piephp;charset=utf8', 'root', '');
      self::$database->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
    catch (Exception $e) {
      die('Erreur : '.$e->getMessage());
    }
    return self::$database;
  }

}

?>
