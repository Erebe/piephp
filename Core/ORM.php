<?php

namespace Core;

class ORM {

  public static function get_structure($table) {
    $request = Database::get()->prepare("DESCRIBE ".$table);
    $request->execute();
    return $request->fetchAll(\PDO::FETCH_COLUMN);
  }

  public static function create($table, $fields) {
    $keys = implode(', ', array_keys($fields));
    $values = ':'.implode(', :', array_keys($fields));
    $request = Database::get()->prepare("INSERT INTO {$table}({$keys})
      VALUES({$values})");
    if ($request->execute($fields))
      return Database::get()->lastInsertId();
      return -1;
  }

  public static function read($table, $id) {
    $request = Database::get()->prepare("SELECT * FROM {$table} WHERE id=:id
      LIMIT 1");
    $request->execute(['id' => $id]);
    $data = $request->fetch();
    return $data;
  }

  public static function find($table, $params =
    array('WHERE' => '1', 'ORDER BY' => 'id ASC', 'LIMIT' => '')) {
    $query = "SELECT * FROM {$table}";
    foreach ($params as $key => $value) {
      $query = $query." {$key} :{$value}";
    }
    $request = Database::get()->prepare($query);
    $request->execute();
    while ($tmp = $request->fetch())
      $datum[] = $tmp;
    $request->closeCursor();
    return $datum;
  }

  public static function update($table, $id, $fields) {
    foreach ($fields as $key => $value)
      $set[] = $key.' = :'.$key;
    $set = implode(', ', $set);
    $request = Database::get()->prepare("UPDATE {$table} SET {$set}
      WHERE id={$id}");
    return $request->execute($fields);
  }

  public static function delete($table, $id) {
    $request = Database::get()->prepare("DELETE FROM {$table} WHERE id={$id}");
    return $request->execute();
  }

  public static function has_one($id, $table1, $table2) {
    $request = Database::get()->prepare("SELECT {$table2}.* FROM {$table2}
        INNER JOIN {$table1} ON {$table2}.id = {$table1}.id_{$table2}
        WHERE {$table1}.id = {$id}");
    $request->execute();
    $data = $request->fetch();
    return $data;
  }

  public static function has_many($id, $table1, $table2) {
    $request = Database::get()->prepare("SELECT {$table2}.* FROM {$table1}
        INNER JOIN {$table2} ON {$table1}.id = {$table2}.id_{$table1}
        WHERE {$table1}.id = {$id}");
    $request->execute();
    $datum = [];
    while ($tmp = $request->fetch())
      $datum[] = $tmp;
    $request->closeCursor();
    return $datum;
  }

  public static function many_to_many($id, $table1, $table2, $througt) {
    $request = Database::get()->prepare("SELECT $table2.* FROM {$througt}
        INNER JOIN {$table2} ON {$table2}.id = {$througt}.id_{$table2}
        INNER JOIN {$table1} ON {$table1}.id = {$througt}.id_{$table1}
        WHERE {$table1}.id = {$id}");
    $request->execute();
    $datum = [];
    while ($tmp = $request->fetch())
      $datum[] = $tmp;
    $request->closeCursor();
    return $datum;
  }

}

?>
