<?php

namespace Core;

class Controller {
  public static $_render;
  public $params;

  public function __construct() {
    $this->params = Request::get_request();
  }
  protected function render($view, $scope = []) {
    extract($scope);
    $f = implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'src', 'View',
      str_replace('Controller', '', basename(get_class($this))), $view]).'.php';
    if (!file_exists($f))
      return (false);
    ob_start();
    include($f);
    $view = ob_get_clean();
    ob_start();
    include(implode(DIRECTORY_SEPARATOR, [dirname(__DIR__),
      'src', 'View', 'index']).'.php');
    $_render = TemplateEngine::execute(ob_get_clean());
    self::$_render = eval("?> $_render <?php ");
    return (true);
  }

  public function __destruct() {
    echo self::$_render;
  }
}

?>
