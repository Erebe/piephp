<?php

namespace Core;

class Core {
  public function run() {
    $call = Router::get($_SERVER['REQUEST_URI']);
    $controller_name =
      '\\Controller\\'.ucfirst($call['controller']).'Controller';
    $controller = new $controller_name();
    $action_name = $call['action'].'Action';
    $controller->$action_name();
  }
}

?>
