<?php

namespace Core;

class TemplateEngine {
  private static $regex = [
    '/{{\s*(.*)\s*}}/',
    '/\@if\s*\((.+)\)/',
    '/\@elseif\s*\((.+)\)/',
    '/\@else/',
    '/\@endif/',
    '/\@foreach\s*\((.+)\)/',
    '/\@endforeach/',
    '/\@isset\((.+)\)/',
    '/\@endisset/',
    '/\@empty\((.+)\)/',
    '/\@endempty/'
  ];
  private static $replacement = [
    '<?= htmlentities($1) ?>',
    '<?php if ($1): ?>',
    '<?php elseif ($1): ?>',
    '<?php else: ?>',
    '<?php endif; ?>',
    '<?php foreach($1): ?>',
    '<?php endforeach; ?>',
    '<?php if (isset($1)): ?>',
    '<?php endif; ?>',
    '<?php if (empty($1)): ?>',
    '<?php endif; ?>'
  ];

  public static function execute($view) {
    return preg_replace(self::$regex, self::$replacement, $view);
  }
}
