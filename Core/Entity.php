<?php

namespace Core;

class Entity {

  private static $has_one_regex = '/^has\s+one\s+([a-zA-Z\_\-]+)$/';
  private static $has_many_regex = '/^has\s+many\s+([a-zA-Z\_\-]+)$/';
  private static $many_to_many_regex =
    '/^many\s+to\s+many\s+([a-zA-Z\_\-]+) througt ([a-zA-Z\_\-]+)$/';

  private $table_name;
  private $fields = [];
  protected static $relations = [];

  public function __construct($params = null) {
    $this->table_name = lcfirst(ltrim(str_replace('Model', '',
      get_class($this)), '\\'));
    $struct = ORM::get_structure($this->table_name);
    foreach ($struct as $value) {
      $this->$value = null;
      $this->fields[] = $value;
    }
    if (!isset($params))
      return ;
    foreach($params as $key => $value) {
      $this->$key = $value;
    }
  }

  private function get_columns() {
    $data = [];
    foreach ($this as $key => $value) {
      if (in_array($key, $this->fields))
        $data[$key] = $value;
    }
    return ($data);
  }

  public function create() {
    $id = ORM::create($this->table_name, $this->get_columns());
    $this->id = $id;
    if ($id < 0)
      return false;
    return true;
  }

  public static function read($id) {
    $table_name = lcfirst(ltrim(str_replace('Model', '', get_called_class()),
      '\\'));
    $data = ORM::read($table_name, $id);
    $class = get_called_class();
    if ($data == false)
      return null;
    $entity = new $class($data);
    $entity->execute_relations();
    return $entity;
  }

  public static function find($params = []) {
    $table_name = lcfirst(ltrim(str_replace('Model', '',
      get_called_class()), '\\'));
    $models = [];
    $data = ORM::find($table_name, $params);
    $class = get_called_class();
    foreach ($data as $datum) {
      $models[] = new $class($datum);
      end($models)->execute_relations();
    }
    return ($models);
  }

  public function update() {
    return ORM::update($this->table_name, $this->id, $this->get_columns());
  }

  public function delete() {
    return ORM::delete($this->table_name, $this->id);
  }

  public function execute_relations() {
    foreach (static::$relations as $relation) {
      if (preg_match(self::$has_one_regex, $relation, $matches))
        $this->has_one($matches[1]);
      else if (preg_match(self::$has_many_regex, $relation, $matches))
        $this->has_many($matches[1]);
      else if (preg_match(self::$many_to_many_regex, $relation, $matches))
        $this->many_to_many($matches[1], $matches[2]);
    }
  }

  private function has_one($table) {
    $model_name = '\\Model\\'.ucfirst($table).'Model';
    $this->$table = new $model_name(ORM::has_one($this->id, $this->table_name,
      $table));
  }

  private function has_many($table) {
    $model_name = '\\Model\\'.ucfirst($table).'Model';
    $data = ORM::has_many($this->id, $this->table_name, $table);
    $this->$table = [];
    foreach ($data as $datum)
      array_push($this->$table, new $model_name($datum));
  }

  private function many_to_many($table, $througt) {
    $model_name = '\\Model\\'.ucfirst($table).'Model';
    $data = ORM::many_to_many($this->id, $this->table_name, $table, $througt);
    $this->$table = [];
    foreach ($data as $datum)
      array_push($this->$table, new $model_name($datum));
  }

}


?>
